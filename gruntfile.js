module.exports = function (grunt) {
  'use strict';

  // jshint camelcase: false
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-jscs');

  grunt.initConfig({

    jscs: {
      app: ['index.js', 'lib/**/*.js', '!lib/**/d3.js'],
      gruntfile: 'gruntfile.js',
      test:      'test/**/*.js',
    },

    jshint: {
      options: {
        jshintrc: true,
      },
      gruntfile: {
        src: 'gruntfile.js',
      },
      app: {
        src: ['index.js', 'lib/**/*.js', '!lib/**/d3.js'],
      },
      test: {
        src: 'test/**/*.js',
      },
    },
  });

  grunt.registerTask('default', ['jshint', 'jscs']);
};
