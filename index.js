'use strict';

var BbPromise = require('bluebird');

var phantom = require('phantom');

var _ = require('lodash');

function PhantomWrapper() {

}

PhantomWrapper.prototype = {
  init: function () {
    var _this = this;

    if (this.phantomHandler) {
      return BbPromise.resolve(this.phantomHandler);
    }

    return BbPromise.fromCallback(function (callback) {
      phantom.create(_.bind(callback, null, null));
    }).then(function (phantomHandler) {
      _this.phantomHandler = phantomHandler;
      return phantomHandler;
    });
  },

  promisifyPage: function (page) {
    var api = ['open', 'evaluate', 'render', 'renderBase64', 'injectJs', 'includeJs'];

    _.each(api, function (method) {
      page[method + 'Async'] = function (firstArg) {
        var args = _.toArray(arguments);

        return BbPromise.fromCallback(function (callback) {
          var cb = function (status) {
            if (status === 'failed') {
              callback(status);
            } else {
              callback(null, status);
            }
          };

          if (firstArg !== undefined) {
            args = [firstArg, cb].concat(_.tail(args));
          } else {
            args = [cb];
          }

          page[method].apply(page, args);
        });
      };
    });

    page.setAsync = function (attr, val) {
      return BbPromise.fromCallback(function (callback) {
        page.set(attr, val, function (status) {
          if (status === 'failed') {
            callback(status);
          } else {
            callback(null, status);
          }
        });
      });
    };
  },

  getPage: function () {
    var _this = this;
    return this.init().then(function (phantomHandler) {
      return BbPromise.fromCallback(function (callback) {
        phantomHandler.createPage(callback.bind(null, null));
      }).then(function (page) {
        _this.promisifyPage(page);

        return page;
      });
    });
  },

};

module.exports = PhantomWrapper;
